
     __  __       _     _ _        __  __             _
    |  \/  |     | |   (_) |      |  \/  |           | |
    | \  / | ___ | |__  _| | ___  | \  / | __ _ _   _| |__   ___ _ __ ___
    | |\/| |/ _ \| '_ \| | |/ _ \ | |\/| |/ _` | | | | '_ \ / _ \ '_ ` _ \
    | |  | | (_) | |_) | | |  __/ | |  | | (_| | |_| | | | |  __/ | | | | |
    |_|  |_|\___/|_.__/|_|_|\___| |_|  |_|\__,_|\__, |_| |_|\___|_| |_| |_|
                                                 __/ |
                                                |___/



## Running the server

```Bash
cd /srv/mobileMayhem
forever start index.js
```
---

## Minigames



### Don't tap the White Tile
> Basically a clone of don't tap the white tile.
> 
> Themes:
> 
> - Piano
> - Dance Floor



### Side Scrolling Shooter
> Accelerometer Controls, Left-to-Right



### Roulette stop



### Themed Impossible Game



### Digging Game

> Themes:
>  - Lottery Scratcher
>  - Priate beach



### Tap to Shoot Ducks
> Duck Hunt?



### Marble Maze
> Like the old wooden ones



### Cookie Clicker
> Notes:
> 
>  - Piano Theme (Instraments in general)
>  - Cheers and boos for win/lose conditions



### Slingshot



### Slap Monkey



### Whack-a-Mole

> Themes:
> 
>  - Whack-a-Gopher
> 
> Notes:
> 
>  - <Gus> I already have an idea and some art done



### Memory Tile Game
> Themes:
>  - Memory Tiles
>  - Grocery Store Isles



### Store Bagger
> Oddly shaped items are given to you to place inside of an oddly shaped bag.
> Every one gets the same bags and items, who packs the most wins.



### Drag the Mouse Maze



### Type Gibberish Game
> Mash the keyboard as fast as you can.



### Spin the Basketball



### Swipe Lock



### Price is Right



### Catch the fruit
> Like the mini game in Kirby 64



### Target Range



### Fill the cup
> Fill a cup to it's maximum size, if it over fill, cup is thrown away and you
> have to start over. Who ever fills the most liquid wins.



### Avoid the Pedestrians



### Run Over Zombies



### Plinko



### Skiball



### Shaker
> Shake the phone as much as possible.


