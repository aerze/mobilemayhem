/*globals Phaser, Firebase */
window.dev;
// var WebFont = {
//     active: false
// }; // Its a thing

window.onload = function () {
    var game = new Phaser.Game(720, 1280, Phaser.CANVAS, 'gameContainer');

    window.dev = game;
    // Loading States
    game.state.add('Boot', MMGame.Boot);
    game.state.add('Preloader', MMGame.Preloader);
    game.state.add('MainMenu', MMGame.MainMenu);
    game.state.add('Menu_Multiplayer', MMGame.Multiplayer);
    game.state.add('Menu_SinglePlayer', MMGame.SinglePlayer);
    game.state.add('Menu_createRoom', MMGame.CreateRoom);
    game.state.add('Minis_ClickTap', MMGame.Minis.ClickTap);
    game.state.add('Minis_SpriteShoot', MMGame.Minis.SpriteShoot);

    game.state.start('Boot');

}