var MMGame = {
    COLORS: {
        green: '#b1d256',
        orange: '#ae4f0e'
    },
    halfWidth: function () {
        return this.game.width / 2;
    },
    Minis: {}
};

MMGame.Boot = function (game) {

};

MMGame.Boot.prototype = {

    init: function () {

        // Only change if we need multiTouch
        this.input.maxPointers = 1;

if (this.game.device.desktop) {
    this.scale.scaleMode = Phaser.ScaleManager.SHOW_ALL;
    var body = document.querySelector('body');
    body.style.width = '400px';
    body.style.margin = '40px auto';

} else {
    this.scale.scaleMode = Phaser.ScaleManager.SHOW_ALL;
    // this.scale.forceLandscape = true;
    // this.scale.forceOrientation(true);
}
this.scale.pageAlignHorizontally = true;
this.scale.pageAlignVertically = true;

        this.game.firebase = this.game.plugins.add(Phaser.Plugin.FirebasePlugin);
        this.game.net = this.game.plugins.add(Phaser.Plugin.NetworkPlugin);
        this.game.Textbox = this.game.plugins.add(Phaser.Plugin.Textbox);
        this.game.Button = this.game.plugins.add(Phaser.Plugin.ButtonPlugin);
    },

    preload: function () {
        this.load.image('preloaderBar', 'assets/preloader-bar.png');
    },

    create: function () {
        this.state.start('Preloader');
    }
}