MMGame.CreateRoom = function (game) {

};

MMGame.CreateRoom.prototype = {

    preload: function () {
        this.stage.backgroundColor = '#b1d256';
        this.load.image('logo', 'assets/logo.png');
    },

    create: function () {
        var halfWidth = this.game.width/2,
            halfHeight = this.game.height/2,
            leftAlign = 100,
            rightAlign = this.game.width - 100,
            boxlength = this.game.width - 200,
            boxheight = 72,
            userFont = {
                font: '50px sans-serif',
                fill: '#000'
            };

        // this.infoText = this.add.

        this.roomnameTextbox = this.add.textbox(
            leftAlign,
            halfHeight - 100,
            boxlength,
            boxheight);

        this.roomnameTextbox.enableKeyboard('text');

        this.continueButton = this.add.button(halfWidth, halfHeight + 150, 'Join Room', 64);
        this.continueButton.alpha = 0;
        this.game.add.tween(this.continueButton).to( { alpha:1 }, 800, Phaser.Easing.Quadratic.In, true, 200);
        this.game.add.tween(this.continueButton).from( { y: -200,  }, 800, Phaser.Easing.Elastic.Out, true);

        this.continueButton.customEvents.customUp.add(function (group) {
        console.log(this.roomnameTextbox.text);
        console.log(this.game.firebase);
        this.game.firebase.setName(this.roomnameTextbox.text, function (err, name, game) {
            if (err === 'EXISTS') {
                console.log('Name taken');
            } else if (err) {
                console.err(err);
            } else {
                console.log(name)
                console.log(game);
            }

        });


            // var tween = this.game.add.tween(this.continueButton).to( { y: -100 }, 400, Phaser.Easing.Quadratic.Out, true);
            // tween.onComplete.add(function () {

            //     this.continueButton.y = 0;
            // }, this);
        }, this);

        this.logo = this.add.sprite(halfWidth, 300, 'logo');
        this.logo.anchor.setTo(0.5, 0.5);

        this.buttonBack = this.add.button(halfWidth, this.game.height - 100, 'Back', 48);
        this.buttonBack.customEvents.customUp.add(function () {
            this.state.start('MainMenu');
        }, this);

    }
};