/*globals MMGame, Phaser, Cocoon */

MMGame.Multiplayer = function (game) {

};

MMGame.Multiplayer.prototype = {

    preload: function () {
        this.stage.backgroundColor = '#79d2c4';
        this.load.image('logo', 'assets/logo.png');
    },

    create: function () {
        var halfWidth = this.game.width/2,
            halfHeight = this.game.height/2,
            leftAlign = 100,
            rightAlign = this.game.width - 100,
            boxlength = this.game.width - 200,
            boxheight = 72;


        this.allGroups = this.add.group();

        this.mainGroup = this.add.group();
        this.game.add.tween(this.mainGroup).from( { y: -200 }, 800, Phaser.Easing.Elastic.Out, true);

        this.button_Back = this.add.button(halfWidth, this.game.height - 100, 'Back', 48, this.mainGroup);
        this.button_Back.customEvents.animComplete.add(function () {

            this.state.start('MainMenu');
            // this.buttonGroup.show();

        }, this);

        this.logo = this.add.sprite(halfWidth, 300, 'logo');
        this.logo.anchor.setTo(0.5, 0.5);





        this.buttonGroup = this.add.group();
        this.allGroups.add(this.buttonGroup);
        this.buttonGroup.hide = function () {
            this.game.add.tween(this).to( { y: -1200 }, 1000, Phaser.Easing.Elastic.Out, true);
        };

        this.buttonGroup.show = function () {
            this.game.add.tween(this).to( {y: 0, x: 0 } , 800, Phaser.Easing.Elastic.Out, true);
        };


        this.joinGame = this.add.button(halfWidth, halfHeight + 150, 'Join Room', 64, this.buttonGroup);
        this.joinGame.customEvents.customUp.add(function (group) {

            // this.buttonGroup.hide();
            // this.toggleButtonGroup();

        }, this);


        this.createGame = this.add.button(halfWidth, (halfHeight + 278), 'Create Room', 64, this.buttonGroup);
        this.createGame.customEvents.customUp.add(function (group) {
            // do somthing on click
        }, this);








        this.loginMenuGroup = this.add.group();
        this.allGroups.add(this.loginMenuGroup);

        this.loginMenuGroup.hide = function () {
            this.game.add.tween(this).to( { y: -1200 }, 1000, Phaser.Easing.Elastic.Out, true);
        };

        this.loginMenuGroup.show = function () {
            this.game.add.tween(this).to( {y: 0, x: 0 } , 800, Phaser.Easing.Elastic.Out, true);
        };

        this.usernameTextbox = this.add.textbox(
            leftAlign,
            500,
            boxlength,
            boxheight,
            'email',
            null,
            this.loginMenuGroup);

        this.passwordTextbox = this.add.textbox(
            leftAlign,
            600,
            boxlength,
            boxheight,
            'password',
            null,
            this.loginMenuGroup);

        this.usernameTextbox.enableKeyboard('text');
        this.passwordTextbox.enableKeyboard('pass');

        this.button_Login = this.add.button(halfWidth, 800, 'Login', 64, this.loginMenuGroup);
        this.button_Login.customEvents.animComplete.add(function () {

            this.game.net.login(this.usernameTextbox.getText(), this.passwordTextbox.getText(),
            function (err, data) {
                console.log(err, data);
            });

        }, this);

        this.button_Signup = this.add.button(halfWidth, 928, 'Sign Up', 64, this.loginMenuGroup);
        this.button_Signup.customEvents.animComplete.add(function () {

            this.game.net.signup(this.usernameTextbox.getText(), this.passwordTextbox.getText(),
            function (err, data) {
                console.log(err, data);
            });

        }, this);


        if (this.game.net.getAuth() === undefined) {
            console.log('User not logged in');

            this.buttonGroup.hide();
            this.loginMenuGroup.show();

        }
    },

    showGroup: function (group) {

        this.allGroups.forEach(function (group) {
            group.hide();
        }, this);

        console.log('show', group);
        group.show();
    },

    toggleButtonGroup: function () {

        // TODO: make a function that can take any group and animate it in
        // while pushing the last one out. maybe showGroup(group);
        if (this.buttonGroup.alive) {

            // move group down
            this.game.add.tween(this.buttonGroup).to( { y: 800 }, 800, Phaser.Easing.Elastic.Out, true);
            this.buttonGroup.alive = false;

            this.game.add.tween(this.loginMenuGroup).to( { y: 800 }, 800, Phaser.Easing.Elastic.Out, true);

            // TODO: update clickable region for each Textbox, should be moved to plugin
            this.loginMenuGroup.forEach(function (child) {
                if (child.clickableRegion) child.clickableRegion.y += 800;
            }, this);

        } else {

            // move group up
            this.game.add.tween(this.buttonGroup).to( { y: 0 }, 800, Phaser.Easing.Elastic.Out, true);
            this.buttonGroup.alive = true;

            this.game.add.tween(this.loginMenuGroup).to( { y: 0 }, 800, Phaser.Easing.Elastic.Out, true);

            // TODO: update clickable region for each Textbox, should be moved to plugin
            this.loginMenuGroup.forEach(function (child) {
                if (child.clickableRegion) child.clickableRegion.y -= 800;
            }, this)
            // this.usernameTextbox.alpha = 0;
        }
    }
};

