/*globals Phaser, Firebase*/

Phaser.Plugin.FirebasePlugin = function (game, parent) {
    Phaser.Plugin.call(this, game, parent);

    this.name = '';

    this.ref = {
        main: new Firebase('https://mmgame.firebaseio.com/'),
        rooms: new Firebase('https://mmgame.firebaseio.com/rooms'),
        players: new Firebase('https://mmgame.firebaseio.com/players'),
        currentRoom: {},
        currentGame: {}
    };

    this.currentRoom = {};
    this.events = {};
    this.player = null;
    // this.events.onRoomChange = new Phaser.Signal();


};


Phaser.Plugin.FirebasePlugin.prototype = Object.create(Phaser.Plugin.prototype);
Phaser.Plugin.FirebasePlugin.prototype.constructor = Phaser.Plugin.FirebasePlugin;

Phaser.Plugin.FirebasePlugin.prototype.setupSignal = function (reference, signalName) {
    this.events[signalName] = new Phaser.Signal();
    reference.on('value', function (snap) {
        this.events[signalName].dispatch(null, snap.val());
    }, function (err) {
        this.events[signalName].dispatch(err, null);
    }, this);
};
Phaser.Plugin.FirebasePlugin.prototype.setupPlayer = function (name) {
    this.player = {
        name: name,
        rank: 0,
    };
    return this.player;
};

Phaser.Plugin.FirebasePlugin.prototype.createRoom = function (name) {
    // if (!this.player.name) throw new Error('Player name not set');

    // this.currentRoom = {
    //     name: name,
    //     gamesToPlay: 10,
    //     players: {
    //         0: this.player
    //     }
    // };

    this.currentRoom = {
        name: name,
        gamesToPlay: 10,
        players: {
            0: {
                name: 'player1'
            }
        }
    };

    this.currentRoom.players['0'].rank = 1;
    this.currentRoom.players['0'].host = true;


    this.ref.currentRoom = this.ref.rooms.child(name);
    this.ref.currentRoom.set(this.currentRoom);
    this.setupSignal(this.ref.currentRoom, 'onRoomChange');

};
Phaser.Plugin.FirebasePlugin.prototype.connectRoom = function (name, callback) {
    this.ref.currentRoom = this.ref.rooms.child(name);
    this.ref.currentRoom.once('value', function (snap) {
        if (snap.exists()) {
            this.setupSignal(this.ref.currentRoom, 'onRoomChange');
            callback(null, true);
        } else {
            callback('NONEXISTANT', false);
        }
    }, function (err) {
        callback(err, false);
    }, this);
};
Phaser.Plugin.FirebasePlugin.prototype.updateRoom = function (room) {
    this.ref.currentRoom.set(room);
};


Phaser.Plugin.FirebasePlugin.prototype.createGame = function (callback) {
    this.currentGame = {
        started: false,
        complete: false,
        players: {
            0: {
                playerName: this.name,
                rank: 1,
                host: true,
                score: 0
            }
        }
    };

    this.ref.currentGame = this.ref.currentRoom.child('currentGame');
    this.ref.currentGame.set(this.currentGame);
    this.setupSignal(this.ref.currentGame, 'onGameChange');

};
Phaser.Plugin.FirebasePlugin.prototype.connectGame = function (callback) {
    this.ref.currentGame = this.ref.currentRoom.child('currentGame');
    this.ref.currentGame.once('value', function (snap) {
        if (snap.exists()) {
            this.setupSignal(this.ref.currentGame, 'onGameChange');
            callback(null, true);
        } else {
            callback('NONEXISTANT', false);
        }
    }, function (err) {
        callback(err, false);
    }, this);
};
Phaser.Plugin.FirebasePlugin.prototype.updateGame = function (game) {
    this.ref.currentGame.set(game);
};

Phaser.Plugin.FirebasePlugin.prototype.addPlayerToRoom = function (room, player) {

};


Phaser.Plugin.FirebasePlugin.prototype.setName = function (name, callback) {
    this.playerRef = this.ref.players.child(name);
    this.playerRef.once('value', function (snap) {
        // if (snap.exists()) {
        //     callback('EXISTS', null);
        //     this.playerRef = {};
        //     return;
        // } else {
            // var createDate = new Date(snap.val()*1000);
            // var now = new Date();
            // var timeDiff = Math.abs(now.getTime() - createDate.getTime());
            // var daysDiff = Math.ceil(timeDiff / (1000 * 3600 * 24));
            // this.log('Days since last log in: ' + )
            // if (daysDiff < 1) callback('TOOSOON', null);
            // else {
                this.playerRef.set(Firebase.ServerValue.TIMESTAMP);
                this.name = name;
                callback(null, name, this.game);
            // }
        // }
    }, function (err) {
        callback(err, null);
    }, this);
};


Phaser.Plugin.FirebasePlugin.prototype.getAuth = function () {
    return this.ref.main.getAuth();

};


Phaser.Plugin.FirebasePlugin.prototype.debug = function (object) {
    console.log('Firebase:');
    console.log(object);
};
Phaser.Plugin.FirebasePlugin.prototype.log = function (object) {
    console.log('Firebase: ' + object);
};


Phaser.Plugin.FirebasePlugin.prototype.signup = function (email, pass, callback) {
    this.ref.main.createUser({
        email: email,
        password: pass
    }, callback);
};






// Phaser.Plugin.FirebasePlugin.prototype.update = function () {
//     if (this.sprite) {
//         this.sprite.x += 0.5;
//     }
// };