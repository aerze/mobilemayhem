'use strict';

var express     = require('express'),
    path        = require('path'),
    morgan      = require('morgan');

var app = new express();
var port = process.env.PORT || 80;
var host = process.env.IP || 'localhost';
if (process.env.ENV === 'dev') {port = 80; host = 'localhost'}


app.use(function(req, res, next) {
  res.header("Access-Control-Allow-Origin", "*");
  res.header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept");
  next();
});

app.use(morgan('dev'));
app.use(express.static(path.join(__dirname, '/mobileMayhem')));

app.listen(port, function() {
    console.log('Server started');
});


